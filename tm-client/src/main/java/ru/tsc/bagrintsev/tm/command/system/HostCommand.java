package ru.tsc.bagrintsev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.system.HostRequest;
import ru.tsc.bagrintsev.tm.dto.response.system.ServerHostResponse;
import ru.tsc.bagrintsev.tm.exception.AbstractException;

import java.net.UnknownHostException;

@Component
public class HostCommand extends AbstractSystemCommand {

    @Override
    public void execute() throws AbstractException, UnknownHostException {
        showOperationInfo();
        @Nullable final ServerHostResponse response = systemEndpoint.getHost(new HostRequest());
        @NotNull final String host = String.format("Current-call server host: %s", response.getHost());
        System.out.println(host);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Print server host.";
    }

    @NotNull
    @Override
    public String getName() {
        return "host";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "-gh";
    }

}
