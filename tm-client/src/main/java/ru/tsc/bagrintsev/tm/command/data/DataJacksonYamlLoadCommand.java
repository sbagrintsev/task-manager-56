package ru.tsc.bagrintsev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.data.DataJacksonYamlLoadRequest;

@Component
public final class DataJacksonYamlLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-yaml";

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        domainEndpoint.loadJacksonYaml(new DataJacksonYamlLoadRequest(getToken()));
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load current application state from yaml file";
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
