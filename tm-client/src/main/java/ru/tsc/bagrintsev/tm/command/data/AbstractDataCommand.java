package ru.tsc.bagrintsev.tm.command.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.enumerated.Role;

@Component
@NoArgsConstructor
public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public static final String FILE_BACKUP = "./backup.base64";

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_JACKSON_XML = "./data.jackson.xml";

    @NotNull
    public static final String FILE_JACKSON_JSON = "./data.jackson.json";

    @NotNull
    public static final String FILE_JACKSON_YAML = "./data.jackson.yaml";

    @NotNull
    public static final String FILE_JAXB_XML = "./data.jaxb.xml";

    @NotNull
    public static final String FILE_JAXB_JSON = "./data.jaxb.json";

    @NotNull
    @Autowired
    protected IDomainEndpoint domainEndpoint;

    @Override
    public @NotNull Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public @NotNull String getShortName() {
        return "";
    }

}
