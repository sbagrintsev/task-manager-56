package ru.tsc.bagrintsev.tm.command.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.system.AboutRequest;
import ru.tsc.bagrintsev.tm.dto.response.system.ServerAboutResponse;

@Component
public class AboutCommand extends AbstractSystemCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        System.out.println("[CLIENT]");
        System.out.printf("Author name: %s%n", propertyService.getAuthorName());
        System.out.printf("E-mail: %s%n", propertyService.getAuthorEmail());
        System.out.println("[SERVER]");
        @NotNull final ServerAboutResponse response = systemEndpoint.getAbout(new AboutRequest());
        System.out.printf("Author name: %s%n", response.getName());
        System.out.printf("E-mail: %s%n", response.getEmail());
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Print about author.";
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "-a";
    }

}
