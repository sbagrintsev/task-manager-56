package ru.tsc.bagrintsev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.data.DataJaxbXmlSaveRequest;

@Component
public final class DataJaxbXmlSaveCommand extends AbstractDataCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        domainEndpoint.saveJaxbXml(new DataJaxbXmlSaveRequest(getToken()));
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save current application state in xml file";
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-jaxb-xml";
    }

}
