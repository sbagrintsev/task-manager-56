package ru.tsc.bagrintsev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.bagrintsev.tm.api.endpoint.IUserEndpoint;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.enumerated.Role;

@Component
public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    @Autowired
    public IAuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    public IUserEndpoint userEndpoint;

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[0];
    }

    @NotNull
    @Override
    public String getShortName() {
        return "";
    }

}
