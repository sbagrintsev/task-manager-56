package ru.tsc.bagrintsev.tm.configuration;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.tsc.bagrintsev.tm.api.endpoint.*;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;

@Configuration
@ComponentScan("ru.tsc.bagrintsev.tm")
@RequiredArgsConstructor
public class ClientConfiguration {

    @NotNull
    private final IPropertyService propertyService;

    @Bean
    @NotNull
    public IAuthEndpoint authEndpoint() {
        return IAuthEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());
    }

    @Bean
    @NotNull
    public IDomainEndpoint domainEndpoint() {
        return IDomainEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());
    }

    @Bean
    @NotNull
    public IProjectEndpoint projectEndpoint() {
        return IProjectEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());
    }

    @Bean
    @NotNull
    public ISystemEndpoint systemEndpoint() {
        return ISystemEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());
    }

    @Bean
    @NotNull
    public ITaskEndpoint taskEndpoint() {
        return ITaskEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());
    }

    @Bean
    @NotNull
    public IUserEndpoint userEndpoint() {
        return IUserEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());
    }

}
