package ru.tsc.bagrintsev.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.task.TaskRemoveByIdRequest;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

@Component
public class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        showParameterInfo(EntityField.ID);
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(getToken());
        request.setTaskId(id);
        taskEndpoint.removeTaskById(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove task by id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-remove";
    }

}
