package ru.tsc.bagrintsev.tm.command.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.system.VersionRequest;

@Component
public class VersionCommand extends AbstractSystemCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        System.out.printf("task-manager-client version: %s%n", propertyService.getApplicationVersion());
        System.out.printf("task-manager-server version: %s%n", systemEndpoint.getVersion(new VersionRequest()).getVersion());
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Print version.";
    }

    @NotNull
    @Override
    public String getName() {
        return "version";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "-v";
    }

}
