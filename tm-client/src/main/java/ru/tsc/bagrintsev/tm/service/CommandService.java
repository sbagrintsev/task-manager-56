package ru.tsc.bagrintsev.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.bagrintsev.tm.api.repository.ICommandRepository;
import ru.tsc.bagrintsev.tm.api.sevice.ICommandService;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.system.EmptyArgumentException;

import java.util.Collection;

@Service
@RequiredArgsConstructor
public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    @Override
    public void add(@Nullable final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAvailableCommands() {
        return commandRepository.getCommandsByName();
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByName(@Nullable final String name) throws AbstractException {
        if (name == null || name.isEmpty()) throw new EmptyArgumentException();
        return commandRepository.getCommandByName(name);
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByShort(final @Nullable String shortName) throws AbstractException {
        if (shortName == null || shortName.isEmpty()) throw new EmptyArgumentException();
        return commandRepository.getCommandByShort(shortName);
    }

    @NotNull
    @Override
    public Iterable<AbstractCommand> getShortCommands() {
        return commandRepository.getCommandsByShort();
    }

}
