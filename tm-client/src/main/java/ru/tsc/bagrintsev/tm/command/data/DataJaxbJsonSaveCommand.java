package ru.tsc.bagrintsev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.data.DataJaxbJsonSaveRequest;

@Component
public final class DataJaxbJsonSaveCommand extends AbstractDataCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        domainEndpoint.saveJaxbJson(new DataJaxbJsonSaveRequest(getToken()));
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save current application state in json file";
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-jaxb-json";
    }

}
