package ru.tsc.bagrintsev.tm.component;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.api.sevice.ICommandService;
import ru.tsc.bagrintsev.tm.api.sevice.ILoggerService;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.util.SystemUtil;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;

@Getter
@Setter
@Component
@RequiredArgsConstructor
public final class Bootstrap {

    private final ICommandService commandService;

    @NotNull
    private final ILoggerService loggerService;

    @Nullable
    private final AbstractCommand[] abstractCommands;

    private boolean continueExecute = true;

    private void initCommands(@Nullable final AbstractCommand[] commands) {
        for (final AbstractCommand command : commands) {
            commandService.add(command);
        }
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager-client.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    @SneakyThrows
    private void processOnStart(@Nullable final String[] args) {
        try {
            if (args == null || args.length == 0) return;
            @Nullable final String arg = args[0];
            processOnStart(arg);
            commandService.getCommandByName("exit").execute();
        } catch (AbstractException | GeneralSecurityException e) {
            loggerService.error(e);
        }
    }

    @SneakyThrows
    private void processOnStart(@Nullable final String arg) {
        @NotNull final AbstractCommand abstractCommand = commandService.getCommandByShort(arg);
        abstractCommand.execute();
    }

    @SneakyThrows
    private void processOnTheGo(@Nullable final String command) {
        @NotNull final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        abstractCommand.execute();
    }

    public void run(@Nullable final String[] args) {
        initCommands(abstractCommands);
        processOnStart(args);
        initPID();
        loggerService.info("*** Welcome to Task Manager ***");
        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));
        while (continueExecute) {
            try {
                System.out.println();
                System.out.println("Enter Command:");
                System.out.print(">> ");
                @NotNull final String command = TerminalUtil.nextLine();
                processOnTheGo(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (Exception e) {
                loggerService.error(e);
                e.printStackTrace();
                System.err.println("[FAIL]");
            }
        }
    }

    @SneakyThrows
    private void shutdown() {
        loggerService.info("*** Task Manager is shutting down... ***");
        stopExecute();
    }

    public void stopExecute() {
        continueExecute = false;
    }

}
