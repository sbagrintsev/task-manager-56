package ru.tsc.bagrintsev.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.model.TaskDTO;
import ru.tsc.bagrintsev.tm.dto.request.task.TaskShowByIdRequest;
import ru.tsc.bagrintsev.tm.dto.response.task.TaskShowByIdResponse;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

@Component
public class TaskShowByIdCommand extends AbstractTaskCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        showParameterInfo(EntityField.ID);
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(getToken());
        request.setTaskId(id);
        @Nullable final TaskShowByIdResponse response = taskEndpoint.showTaskById(request);
        @Nullable final TaskDTO task = response.getTask();
        if (task != null) {
            showTask(task);
        }
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show task by id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show";
    }

}
