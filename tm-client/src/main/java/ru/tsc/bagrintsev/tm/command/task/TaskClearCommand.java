package ru.tsc.bagrintsev.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.task.TaskClearRequest;

@Component
public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        taskEndpoint.clearTask(new TaskClearRequest(getToken()));
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

}
