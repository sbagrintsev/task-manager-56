package ru.tsc.bagrintsev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.bagrintsev.tm.api.sevice.ICommandService;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.enumerated.Role;

@Component
public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    @Autowired
    public ICommandService commandService;

    @Autowired
    @NotNull
    public IPropertyService propertyService;

    @Autowired
    @NotNull
    public ISystemEndpoint systemEndpoint;

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[0];
    }

}
