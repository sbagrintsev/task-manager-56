package ru.tsc.bagrintsev.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.model.TaskDTO;
import ru.tsc.bagrintsev.tm.dto.request.task.TaskListRequest;
import ru.tsc.bagrintsev.tm.dto.response.task.TaskListResponse;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public class TaskListCommand extends AbstractTaskCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        showParameterInfo(EntityField.SORT);
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sortValue = TerminalUtil.nextLine();
        @NotNull final TaskListRequest request = new TaskListRequest(getToken());
        request.setSortValue(sortValue);
        @Nullable final TaskListResponse response = taskEndpoint.listTask(request);
        @Nullable final List<TaskDTO> tasks = response.getTasks();
        if (tasks != null) {
            tasks.forEach(System.out::println);
        }
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Print task list.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

}
