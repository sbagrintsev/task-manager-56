package ru.tsc.bagrintsev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.bagrintsev.tm.component.Bootstrap;
import ru.tsc.bagrintsev.tm.configuration.ClientConfiguration;

/**
 * @author Sergey Bagrintsev
 */

public final class Application {

    public static void main(@Nullable final String[] args) {
        try (@NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ClientConfiguration.class)) {
            @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
            bootstrap.run(args);
        }
    }

}
