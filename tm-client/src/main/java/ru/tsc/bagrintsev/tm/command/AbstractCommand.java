package ru.tsc.bagrintsev.tm.command;

import jakarta.xml.bind.JAXBException;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.api.model.ICommand;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.service.TokenService;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Locale;

@Component
@NoArgsConstructor
public abstract class AbstractCommand implements ICommand {

    @NotNull
    @Autowired
    protected TokenService tokenService;

    public abstract void execute() throws IOException, AbstractException, GeneralSecurityException, ClassNotFoundException, JAXBException;

    @NotNull
    public abstract String getDescription();

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract Role[] getRoles();

    @NotNull
    public abstract String getShortName();

    @SneakyThrows
    @Nullable
    protected String getToken() {
        return tokenService.getToken();
    }

    @SneakyThrows
    protected void setToken(@Nullable final String token) {
        if (token == null || token.isEmpty()) return;
        tokenService.setToken(token);
    }

    protected void showOperationInfo() {
        System.out.printf("[%s]%n", getName().toUpperCase(Locale.ROOT).replace('-', ' '));
    }

    protected void showParameterInfo(@NotNull final EntityField parameter) {
        System.out.printf("Enter %s: %n", parameter.getDisplayName());
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull StringBuilder result = new StringBuilder();
        result.append("[");
        result.append(String.format("%-35s", getName()));
        result.append(" | ");
        result.append(String.format("%-25s", getShortName()));
        result.append("]");
        if (!getDescription().isEmpty()) {
            while (result.length() < 75) {
                result.append(" ");
            }
            result.append(getDescription());
        }
        return result.toString();
    }

}
