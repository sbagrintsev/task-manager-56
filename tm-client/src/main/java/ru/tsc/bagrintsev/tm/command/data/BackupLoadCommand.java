package ru.tsc.bagrintsev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.data.BackupLoadRequest;

@Component
public final class BackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "backup-load";

    @Override
    @SneakyThrows
    public void execute() {
        domainEndpoint.loadBackup(new BackupLoadRequest(getToken()));
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load current application state from backup";
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
