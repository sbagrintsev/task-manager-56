package ru.tsc.bagrintsev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.dto.model.TaskDTO;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.util.DateUtil;

@Component
public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    @Autowired
    public ITaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getShortName() {
        return "";
    }

    protected void showTask(final TaskDTO task) {
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(task.getDateCreated()));
        System.out.println("STARTED: " + DateUtil.toString(task.getDateStarted()));
        System.out.println("FINISHED: " + DateUtil.toString(task.getDateFinished()));
    }

}
