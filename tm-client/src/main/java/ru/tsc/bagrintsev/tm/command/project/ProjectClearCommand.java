package ru.tsc.bagrintsev.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.project.ProjectClearRequest;

@Component
public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        projectEndpoint.clearProject(new ProjectClearRequest(getToken()));
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

}
