package ru.tsc.bagrintsev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskRepository extends IWBSRepository<Task> {

    @Override
    void add(@NotNull final Task record);

    @Override
    void addAll(@NotNull final Collection<Task> records);

    @Override
    void clear(@NotNull final String userId);

    @Override
    void clearAll();

    @Override
    boolean existsById(
            @NotNull final String userId,
            @NotNull final String id
    );

    @Override
    @Nullable
    List<Task> findAll();

    @Nullable
    List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    );

    @Override
    @Nullable
    List<Task> findAllByUserId(@NotNull final String userId);

    @Override
    @Nullable
    List<Task> findAllSort(
            @NotNull final String userId,
            @NotNull final String order
    );

    @Override
    @Nullable
    Task findOneById(
            @NotNull final String userId,
            @NotNull final String id
    );

    @Override
    void removeById(
            @NotNull final String userId,
            @NotNull final String id
    );

    void setProjectId(
            @NotNull final String userId,
            @NotNull final String taskId,
            @Nullable final Project project
    );

    @Override
    long totalCount();

    @Override
    long totalCountByUserId(@NotNull final String userId);

    @Override
    void update(@NotNull final Task task);

    @Override
    void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    );

}
