package ru.tsc.bagrintsev.tm.component;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.EntityManagerFactory;
import jakarta.xml.ws.Endpoint;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.api.sevice.ILoggerService;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;
import ru.tsc.bagrintsev.tm.api.sevice.ISenderService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IUserServiceDTO;
import ru.tsc.bagrintsev.tm.endpoint.AbstractEndpoint;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.listener.EntityListener;
import ru.tsc.bagrintsev.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.Arrays;

@Getter
@Component
@RequiredArgsConstructor
public final class Bootstrap {

    @NotNull
    private final ISenderService senderService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IUserServiceDTO userService;

    @NotNull
    private final ILoggerService loggerService;

    @NotNull
    private final EntityManagerFactory factory;

    @NotNull
    private final AbstractEndpoint[] abstractEndpoints;

    @NotNull
    private final Backup backup;

    private void endpointRegistry(@NotNull final AbstractEndpoint endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = String.format("http://%s:%s/%s?wsdl", host, port, name);
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    private void initEndpoints() {
        Arrays.stream(abstractEndpoints).forEach(this::endpointRegistry);
    }

    private void initJMSListeners() {
        final EntityListener listener = new EntityListener(senderService);
        final SessionFactoryImpl sessionFactory = factory.unwrap(SessionFactoryImpl.class);
        final EventListenerRegistry listenerRegistry = sessionFactory.getServiceRegistry().getService(EventListenerRegistry.class);
        listenerRegistry.getEventListenerGroup(EventType.POST_DELETE).appendListener(listener);
        listenerRegistry.getEventListenerGroup(EventType.POST_INSERT).appendListener(listener);
        listenerRegistry.getEventListenerGroup(EventType.POST_UPDATE).appendListener(listener);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initUsers() {
        boolean exists;
        try {
            userService.findByLogin("admin");
            exists = true;
        } catch (AbstractException e) {
            exists = false;
        }
        if (!exists) {
            try {
                userService.create("admin", "admin");
                userService.setRole("admin", Role.ADMIN);
            } catch (GeneralSecurityException | AbstractException e) {
                System.err.println("User initialization error...");
                loggerService.error(e);
            }
        }
    }

    public void run() throws Exception {
        initPID();
        initEndpoints();
        initUsers();
        initJMSListeners();
        loggerService.info("*** Task Manager Server Started ***");
        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));
//        backup.start();
    }

    @SneakyThrows
    private void shutdown() {
        loggerService.info("*** Task Manager Server Stopped ***");
        factory.close();
        senderService.stop();
        senderService.stopJMS();
//        backup.stop();
    }

}
