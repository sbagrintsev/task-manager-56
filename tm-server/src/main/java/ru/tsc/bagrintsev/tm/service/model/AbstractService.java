package ru.tsc.bagrintsev.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.tsc.bagrintsev.tm.api.repository.model.IAbstractRepository;
import ru.tsc.bagrintsev.tm.api.sevice.model.IAbstractService;
import ru.tsc.bagrintsev.tm.model.AbstractModel;

import java.util.Collection;
import java.util.List;

@Service
public abstract class AbstractService<M extends AbstractModel, R extends IAbstractRepository<M>> implements IAbstractService<M> {

    @NotNull
    public ApplicationContext context;

    @NotNull
    public abstract List<M> findAll();

    @NotNull
    public abstract Collection<M> set(@NotNull final Collection<M> records);

    @Autowired
    void setContext(@NotNull final ApplicationContext context) {
        this.context = context;
    }

    public abstract long totalCount();

}
