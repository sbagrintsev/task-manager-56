package ru.tsc.bagrintsev.tm.service.model;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.bagrintsev.tm.api.repository.model.IProjectRepository;
import ru.tsc.bagrintsev.tm.api.repository.model.ITaskRepository;
import ru.tsc.bagrintsev.tm.api.repository.model.IUserRepository;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;
import ru.tsc.bagrintsev.tm.api.sevice.model.IUserService;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.entity.IncorrectRoleException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.EmailIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectParameterNameException;
import ru.tsc.bagrintsev.tm.exception.user.*;
import ru.tsc.bagrintsev.tm.model.User;
import ru.tsc.bagrintsev.tm.util.HashUtil;

import java.security.GeneralSecurityException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @Getter
    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    @Override
    public User checkUser(
            @Nullable final String login,
            @Nullable final String password
    ) throws PasswordIsIncorrectException, AccessDeniedException, LoginIsIncorrectException, GeneralSecurityException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        if (password == null || password.isEmpty()) throw new PasswordIsIncorrectException();
        @NotNull final User user = findByLogin(login);
        if (user.getLocked()) throw new AccessDeniedException();
        @NotNull final Integer iterations = propertyService.getPasswordHashIterations();
        @NotNull final Integer keyLength = propertyService.getPasswordHashKeyLength();
        if (!(HashUtil.generateHash(password, user.getPasswordSalt(), iterations, keyLength)).equals(user.getPasswordHash())) {
            throw new PasswordIsIncorrectException();
        }
        return user;
    }

    @Override
    public void clearAll() {
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            repository.clearAll();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) throws GeneralSecurityException, PasswordIsIncorrectException, LoginIsIncorrectException, LoginAlreadyExistsException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        if (isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        if (password == null || password.isEmpty()) throw new PasswordIsIncorrectException();
        @NotNull final Integer iterations = getPropertyService().getPasswordHashIterations();
        @NotNull final Integer keyLength = getPropertyService().getPasswordHashKeyLength();
        final byte @NotNull [] salt = HashUtil.generateSalt();
        @NotNull final String passwordHash = HashUtil.generateHash(password, salt, iterations, keyLength);
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        @NotNull User user = new User(login, passwordHash, salt);
        try {
            repository.add(user);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public List<User> findAll() {
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final List<User> result = repository.findAll();
            return (result == null) ? Collections.emptyList() : result;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User findByEmail(@Nullable final String email) throws EmailIsEmptyException, UserNotFoundException {
        if (email == null || email.isEmpty()) throw new EmailIsEmptyException();
        @Nullable User user;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            user = repository.findByEmail(email);
            if (user == null) throw new UserNotFoundException();
            return user;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User findByLogin(@Nullable final String login) throws LoginIsIncorrectException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        @Nullable User user;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            return user;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull User findOneById(@Nullable String id) throws IdIsEmptyException, UserNotFoundException {
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        @Nullable User user;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            user = repository.findOneById(id);
            if (user == null) throw new UserNotFoundException();
            return user;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    private IProjectRepository getProjectRepository(@NotNull final EntityManager entityManager) {
        @NotNull final IProjectRepository projectRepository = context.getBean(IProjectRepository.class);
        projectRepository.setEntityManager(entityManager);
        return projectRepository;
    }

    @NotNull
    private IUserRepository getRepository() {
        return context.getBean(IUserRepository.class);
    }

    @NotNull
    private ITaskRepository getTaskRepository(@NotNull final EntityManager entityManager) {
        @NotNull final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        taskRepository.setEntityManager(entityManager);
        return taskRepository;
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.isEmailExists(email);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.isLoginExists(login);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) throws LoginIsIncorrectException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @Nullable final User user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(true);
            repository.setParameter(user);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User removeByLogin(@Nullable final String login) throws LoginIsIncorrectException, IdIsEmptyException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        @Nullable User user;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final ITaskRepository taskRepository = getTaskRepository(entityManager);
            @NotNull final IProjectRepository projectRepository = getProjectRepository(entityManager);
            user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            @NotNull final String userId = user.getId();
            taskRepository.clear(userId);
            projectRepository.clear(userId);
            repository.removeByLogin(login);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public @NotNull Collection<User> set(@NotNull Collection<User> records) {
        if (records.isEmpty()) return records;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            repository.addAll(records);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return records;
    }

    @NotNull
    @Override
    public User setParameter(
            @Nullable final User user,
            @NotNull final EntityField paramName,
            @Nullable final String paramValue
    ) throws EmailAlreadyExistsException, UserNotFoundException, IncorrectParameterNameException {
        if (user == null) throw new UserNotFoundException();
        if (EntityField.EMAIL.equals(paramName) && isEmailExists(paramValue)) {
            throw new EmailAlreadyExistsException(paramValue);
        }
        @Nullable User userResult;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            switch (paramName) {
                case EMAIL:
                    user.setEmail(paramValue);
                    break;
                case FIRST_NAME:
                    user.setFirstName(paramValue);
                    break;
                case MIDDLE_NAME:
                    user.setMiddleName(paramValue);
                    break;
                case LAST_NAME:
                    user.setLastName(paramValue);
                    break;
                case LOCKED:
                    user.setLocked("true".equals(paramValue));
                    break;
                default:
                    throw new IncorrectParameterNameException(paramName, "User");
            }
            repository.setParameter(user);
            transaction.commit();
            userResult = repository.findOneById(user.getId());
            if (userResult == null) throw new UserNotFoundException();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return userResult;
    }

    @NotNull
    @Override
    public User setPassword(
            @Nullable final String userId,
            @Nullable final String newPassword,
            @Nullable final String oldPassword
    ) throws GeneralSecurityException, PasswordIsIncorrectException, IdIsEmptyException, AccessDeniedException, LoginIsIncorrectException, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (newPassword == null || newPassword.isEmpty()) throw new PasswordIsIncorrectException();
        if (oldPassword == null || oldPassword.isEmpty()) throw new PasswordIsIncorrectException();
        @NotNull final Integer iterations = getPropertyService().getPasswordHashIterations();
        @NotNull final Integer keyLength = getPropertyService().getPasswordHashKeyLength();
        final byte @NotNull [] salt = HashUtil.generateSalt();
        @NotNull final String passwordHash = HashUtil.generateHash(newPassword, salt, iterations, keyLength);
        @Nullable User user;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            user = repository.findOneById(userId);
            if (user == null) throw new UserNotFoundException();
            checkUser(user.getLogin(), oldPassword);
            repository.setUserPassword(user.getLogin(), passwordHash, salt);
            transaction.commit();
            user = repository.findOneById(userId);
            if (user == null) throw new UserNotFoundException();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User setRole(
            @Nullable final String login,
            @Nullable final Role role
    ) throws IncorrectRoleException, LoginIsIncorrectException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        if (role == null) throw new IncorrectRoleException();
        @Nullable User user;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            repository.setRole(login, role);
            user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public long totalCount() {
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.totalCount();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) throws LoginIsIncorrectException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @Nullable final User user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(false);
            repository.setParameter(user);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws IdIsEmptyException, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        @Nullable User user;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            user = repository.findOneById(userId);
            if (user == null) throw new UserNotFoundException();
            user.setFirstName(firstName);
            user.setMiddleName(middleName);
            user.setLastName(lastName);
            repository.setParameter(user);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

}
