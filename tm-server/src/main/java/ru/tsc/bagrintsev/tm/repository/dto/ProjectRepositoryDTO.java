package ru.tsc.bagrintsev.tm.repository.dto;

import jakarta.persistence.EntityManager;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.bagrintsev.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.tsc.bagrintsev.tm.dto.model.ProjectDTO;

@Repository
@Scope("prototype")
public class ProjectRepositoryDTO extends UserOwnedRepositoryDTO<ProjectDTO> implements IProjectRepositoryDTO {

    public ProjectRepositoryDTO(
            @NotNull final EntityManager entityManager
    ) {
        super(ProjectDTO.class, entityManager);
    }

}
