package ru.tsc.bagrintsev.tm.api.sevice.dto;

import ru.tsc.bagrintsev.tm.dto.model.ProjectDTO;

public interface IProjectServiceDTO extends IUserOwnedServiceDTO<ProjectDTO> {

}
