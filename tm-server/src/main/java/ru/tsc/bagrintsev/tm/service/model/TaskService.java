package ru.tsc.bagrintsev.tm.service.model;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.bagrintsev.tm.api.repository.model.IProjectRepository;
import ru.tsc.bagrintsev.tm.api.repository.model.ITaskRepository;
import ru.tsc.bagrintsev.tm.api.repository.model.IUserRepository;
import ru.tsc.bagrintsev.tm.api.sevice.model.ITaskService;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.DescriptionIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectStatusException;
import ru.tsc.bagrintsev.tm.exception.field.NameIsEmptyException;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.bagrintsev.tm.model.User;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    @Override
    public @NotNull Task add(
            @Nullable final String userId,
            @Nullable final Task task
    ) throws ModelNotFoundException, IdIsEmptyException, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (task == null) throw new ModelNotFoundException();
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final IUserRepository userRepository = getUserRepository(entityManager);
            @Nullable final User user = userRepository.findOneById(userId);
            if (user == null) throw new UserNotFoundException();
            task.setUser(user);
            repository.add(task);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public @NotNull Task changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws IncorrectStatusException, IdIsEmptyException, TaskNotFoundException, ModelNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        if (status == null) throw new IncorrectStatusException();
        @Nullable Task task;
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            task = repository.findOneById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            if (status.equals(Status.IN_PROGRESS)) {
                task.setStatus(status);
                task.setDateStarted(new Date());
            } else if (status.equals(Status.COMPLETED)) {
                task.setStatus(status);
                task.setDateFinished(new Date());
            } else if (status.equals(Status.NOT_STARTED)) {
                task.setStatus(status);
                task.setDateStarted(null);
                task.setDateFinished(null);
            }
            update(task);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public void clear(@Nullable final String userId) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            repository.clear(userId);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clearAll() {
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            repository.clearAll();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull Task create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws NameIsEmptyException, IdIsEmptyException, ModelNotFoundException, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        @NotNull final Task task = new Task();
        task.setName(name);
        add(userId, task);
        return task;
    }

    @Override
    public @NotNull Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws DescriptionIsEmptyException, IdIsEmptyException, NameIsEmptyException, ModelNotFoundException, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionIsEmptyException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        add(userId, task);
        return task;
    }

    @Override
    public boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.existsById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final List<Task> result = repository.findAll();
            return (result == null) ? Collections.emptyList() : result;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull List<Task> findAll(@Nullable final String userId) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            List<Task> list = repository.findAllByUserId(userId);
            return list == null ? Collections.emptyList() : list;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull List<Task> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (sort == null) {
            return findAll(userId);
        }
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @NotNull final String order = getQueryOrder(sort);
            List<Task> list = repository.findAllSort(userId, order);
            return list == null ? Collections.emptyList() : list;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (projectId == null || projectId.isEmpty())
            throw new IdIsEmptyException(EntityField.PROJECT_ID.getDisplayName());
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            List<Task> list = repository.findAllByProjectId(userId, projectId);
            return list == null ? Collections.emptyList() : list;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Task findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException, TaskNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable Task task = repository.findOneById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            return task;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    private IProjectRepository getProjectRepository(@NotNull final EntityManager entityManager) {
        @NotNull final IProjectRepository projectRepository = context.getBean(IProjectRepository.class);
        projectRepository.setEntityManager(entityManager);
        return projectRepository;
    }

    @NotNull
    private ITaskRepository getRepository() {
        return context.getBean(ITaskRepository.class);
    }

    @NotNull
    private IUserRepository getUserRepository(@NotNull final EntityManager entityManager) {
        @NotNull final IUserRepository userRepository = context.getBean(IUserRepository.class);
        userRepository.setEntityManager(entityManager);
        return userRepository;
    }

    @Override
    public @NotNull Task removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException, TaskNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        @Nullable Task task;
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            task = repository.findOneById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            repository.removeById(userId, id);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public @NotNull Collection<Task> set(@NotNull final Collection<Task> tasks) {
        if (tasks.isEmpty()) return tasks;
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            repository.addAll(tasks);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return tasks;
    }

    @Override
    public @NotNull Task setProjectId(
            @NotNull final String userId,
            @NotNull final String taskId,
            @Nullable final String projectId
    ) throws TaskNotFoundException, IdIsEmptyException, ProjectNotFoundException {
        @Nullable Task task;
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(entityManager);
            @Nullable final Project project = projectId == null ? null : projectRepository.findOneById(projectId);
            repository.setProjectId(userId, taskId, project);
            transaction.commit();
            task = repository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public long totalCount() {
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.totalCount();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long totalCount(@Nullable final String userId) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.totalCountByUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@Nullable final Task task) throws ModelNotFoundException {
        if (task == null) throw new ModelNotFoundException();
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            repository.update(task);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws DescriptionIsEmptyException, NameIsEmptyException, IdIsEmptyException, TaskNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionIsEmptyException();
        @Nullable Task task;
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            repository.updateById(userId, id, name, description);
            transaction.commit();
            task = repository.findOneById(userId, id);
            if (task == null) throw new TaskNotFoundException();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

}
