package ru.tsc.sbagrintsev.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.dto.model.ProjectDTO;
import ru.tsc.bagrintsev.tm.dto.model.TaskDTO;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.service.dto.ProjectTaskServiceDTO;
import ru.tsc.sbagrintsev.tm.marker.DBCategory;

@Category(DBCategory.class)
public final class ProjectTaskServiceDTOTest extends AbstractDTOTest {

    @NotNull
    private final String userId = "testUserId1";

    @NotNull
    private ProjectTaskServiceDTO projectTaskServiceDTO;

    @Before
    public void setUp() throws ModelNotFoundException, IdIsEmptyException {
        projectTaskServiceDTO = new ProjectTaskServiceDTO(projectService, taskService);
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setId("projectId1");
        project.setName("projectName1");
        projectService.add(userId, project);
        @NotNull final TaskDTO task = new TaskDTO();
        task.setId("taskId1");
        task.setName("taskName1");
        taskService.add(userId, task);
    }

    @Test
    @Category(DBCategory.class)
    public void testBindTaskToProject() throws AbstractException {
        Assert.assertNull(taskService.findAll().get(0).getProjectId());
        projectTaskServiceDTO.bindTaskToProject(userId, "projectId1", "taskId1");
        Assert.assertEquals("projectId1", taskService.findOneById(userId, "taskId1").getProjectId());
    }

    @Test
    @Category(DBCategory.class)
    public void testRemoveProjectById() throws AbstractException {
        Assert.assertFalse(projectService.findAll().isEmpty());
        Assert.assertFalse(taskService.findAll().isEmpty());
        projectTaskServiceDTO.bindTaskToProject(userId, "projectId1", "taskId1");
        projectTaskServiceDTO.removeProjectById(userId, "projectId1");
        Assert.assertTrue(projectService.findAll().isEmpty());
        Assert.assertTrue(taskService.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void testUnbindTaskFromProject() throws AbstractException {
        projectTaskServiceDTO.bindTaskToProject(userId, "projectId1", "taskId1");
        Assert.assertEquals("projectId1", taskService.findOneById(userId, "taskId1").getProjectId());
        projectTaskServiceDTO.unbindTaskFromProject(userId, "taskId1");
        Assert.assertNull(taskService.findAll().get(0).getProjectId());
    }

}
