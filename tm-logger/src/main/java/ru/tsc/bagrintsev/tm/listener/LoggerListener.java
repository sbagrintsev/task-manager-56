package ru.tsc.bagrintsev.tm.listener;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.api.sevice.ILoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Component
@RequiredArgsConstructor
public class LoggerListener implements MessageListener {

    @NotNull
    private final ILoggerService loggerService;

    @Override
    @SneakyThrows
    public void onMessage(@NotNull final Message message) {
        if (!(message instanceof TextMessage)) return;
        @NotNull final TextMessage textMessage = (TextMessage) message;
        @NotNull final String json = textMessage.getText();
        loggerService.logToMongo(json);
    }

}
