package ru.tsc.bagrintsev.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
public enum Entity {
    ABSTRACT("abstract"),
    TASK("task"),
    PROJECT("project"),
    USER("user"),
    ROLE("role"),
    STATUS("status");

    @NotNull
    private final String displayName;

    Entity(@NotNull final String displayName) {
        this.displayName = displayName;
    }

}
