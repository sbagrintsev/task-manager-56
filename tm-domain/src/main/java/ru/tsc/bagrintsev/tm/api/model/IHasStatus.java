package ru.tsc.bagrintsev.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull final Status status);

}
