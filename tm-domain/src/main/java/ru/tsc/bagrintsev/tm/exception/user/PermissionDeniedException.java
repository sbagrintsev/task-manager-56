package ru.tsc.bagrintsev.tm.exception.user;

public class PermissionDeniedException extends AbstractUserException {

    public PermissionDeniedException() {
        super("Permission denied! User access level is not enough...");
    }

}
