package ru.tsc.bagrintsev.tm.dto.request.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public final class UserSignOutRequest extends AbstractUserRequest {

    public UserSignOutRequest(@Nullable String token) {
        super(token);
    }

}
